---
title: "Week 7. Modelling: A Preview"
date: 2020-04-16T08:18:13+08:00
draft: false
tags: [model]
---

<img src="https://uploads2.wikiart.org/images/pablo-picasso/painter-and-his-model-1928.jpg!Large.jpg" style="zoom:67%;" />

## 定義與概念

定義不是來困惑你的，而是引導區分一些不同的概念。他們常被有意的設計成與一般用詞有差別，以避免你從日常意義去理解他們。因此，給予這些定義應有的尊重與理解，可以幫助你進入不同的異世界。關於modelling 我們可以從幾個基本設定開始了解

1. Purpose: Explanation and Prediction. Explain what has happened and predict what will happen. Observed vs. Unobserved; Known vs. Unknown; Past/Now vs. Future
2. Population and Sample
3. Overfitting and unrepeatable science/results. Training vs. Testing (data)
4. Diagnosis and remedies: Withholding some data for "testing" such as cross-validation or random subsample. Restriction on  model class.  
5. Model as a relation between or among variables. Continuous vs. Continuous; Continuous vs. Categorical; Categorical vs. Categorical; Numerical and Graphical display/exploration. 