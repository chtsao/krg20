---
title: "Project Presentation"
date: 2020-05-28T04:38:28+08:00
draft: false
tags: [adm, team, project, presentation]
---
![](https://img.4gamers.com.tw/ckfinder/files/Elvis/News/20200123-Pokemon/gobattleleague-announcement-2.jpg)
# 期末報告 (課堂報告)

1. 6/08. 2 vs 3？ --NBA三分球趨勢分析 ([pres](https://chtsao.gitlab.io/krg20/p.2v3.html), [rept](https://chtsao.gitlab.io/krg20/r.12321.html)): 8.TBA: <u>蘇羿豪</u>, 阮新宇, 林穎宏 
2. 6/08. 如何拿高薪？--NBA薪資分析 ([pres](https://chtsao.gitlab.io/krg20/p.nbs.salary.html), [rept](https://chtsao.gitlab.io/krg20/r.work.html)): 10.高粱王: 張軼堂, 劉群益, 林士玄, <u>林泓廷</u>
3. 6/08. 哪裡最划算？--匯率與旅遊意願分析 ([pres](https://chtsao.gitlab.io/krg20/p.travelvrate.html), [rept](https://chtsao.gitlab.io/krg20/r.哪裡最划算？–匯率與旅遊意願分析.html)): 3.今日打老虎 明日打老母: <u>王品皓</u>,  <u>童政瑋</u>, 尹健璋,  黃紫瑄, 莊芸綺, 劉心雨
4. 6/08. Who and Where--旅遊地點偏好分析 ([pres](https://chtsao.gitlab.io/krg20/p.旅遊去哪裡.html), [rept](https://chtsao.gitlab.io/krg20/r.我們就爛組.html)): 1.我們就爛:  吳晨瑄,  紀婷翊, <u>白敏蒨</u>, 吳姿儀
5. 6/10. 新聞追追追--休退學率探究 ([pres](https://chtsao.gitlab.io/krg20/p.休退學.html), [rept](https://chtsao.gitlab.io/krg20/r.新聞追追追–休退學率探究.html)) 7. 康暐郡, <u>吳昱潔</u>, 高偉泰, 蕭冠鈞
6. 6/10. 書海尋奇: 晉江排名([pres](https://chtsao.gitlab.io/krg20/p.晉江排名.html), [rept]((https://chtsao.gitlab.io/krg20/r.暑期書荒.html))).  13.李采晏
8. 6/15. BGG桌游評分分析 ([pres](https://chtsao.gitlab.io/krg20/p.boardgameanalysis.html), [rept](https://chtsao.gitlab.io/krg20/r.bgg.analysis.html)):  2.IU是天使: 沈怡姍, 蔡瑄, 曾柏恩, 彭長元, <u>陳慬瑜</u>, 林羿君, 高芷芃
9. 6/15. GDP與教育普及率  ([pres](https://chtsao.gitlab.io/krg20/p.eduvgdp.html), [rept](https://chtsao.gitlab.io/krg20/r.eduaction.html)):5.蔡濬宇的三個室友與凱倫: <u>吳振瑋</u>, 吳林晏, 黃宗元, 羅凱倫 
10. 6/15.  寶可夢分析 ([pres](https://chtsao.gitlab.io/krg20/p.Pokemon.html), [rept](https://chtsao.gitlab.io/krg20/r.pokemon.nb.html)):11.鼠巴拉希: 陳貞佑, <u>葉庭</u>, 楊智凱, 林思佑, 廖政維, 施承翰
11. 6/17. 誰是薪水小偷？ ([pres](https://chtsao.gitlab.io/krg20/p.誰是薪水小偷.html), [rept](https://chtsao.gitlab.io/krg20/r.神魔統軟.html)) 6.神魔統軟:  <u>邵平遠</u>,  阮俊瑋, 謝旻倫, 謝昕龍, 王委凱
12. 6/17. 四大超商比較  ([pres](https://chtsao.gitlab.io/krg20/p.四大超商比較.html), [rept](https://chtsao.gitlab.io/krg20/r.書面報告_全台四大超商分布.pdf)). 9.TBA: <u>吳家萱</u>, 宋宸耘, 黃子娟, 郭郁琪. 主題：全臺灣(含離島)四大超商的分布 

#### 改書面報告
* [動漫評分](https://chtsao.gitlab.io/krg20/r.team4動漫評分.html) 4.TBA:  詹政樺 , 陳心如,  <u>龍冠邑</u>, 黃奕淳,
* [給阿姨們的簡單易懂的乳癌預後狀況分析](https://chtsao.gitlab.io/krg20/r.給阿姨們的簡單易懂的乳癌風險分析.html )。 12.賴宜婷 


### 說明

* **課堂報告**：時間原則 20 min, 硬體/Q&A: 3 min.。參考 Rmarkdown 模例[html](https://chtsao.gitlab.io/krg20/lalabear.html), [rmd](https://chtsao.gitlab.io/krg20/lalabear.Rmd)。
* **書面報告**：6/21 23：59 前寄至我電郵信箱，以 pdf, html 檔為偏好格式。[架構參考](https://chtsao.gitlab.io/krg20/proj.template.nb.html)。
* 不在編組名單內小組/同學以繳交書面/電子報告為原則。如有高度課堂報告意願請儘快與我聯繫。如上列13 小組沒有課堂報告意願，也請在表訂報告7天前告知，以便安排。
* （新）Kno 狗尾續貂, 試著幫已報告各組加上標題, 如果不適合或有另外更好建議，請讓我知道修正。另外，徵求各組同意後，我會將課堂報告以及最後書面報告放在課網連結，提供有興趣的閱者參考。

### R Markdown Links (New)

* [Lgatto's slide template](https://github.com/lgatto/slide-templates)
* [uslides by mkearney](https://github.com/mkearney/uslides)
* [R Markdown ioslides presentation](https://bookdown.org/yihui/rmarkdown/ioslides-presentation.html) from [R Markdown: The Definitive Guide](https://bookdown.org/yihui/rmarkdown/)
* [Gallery from Rstudio](https://rmarkdown.rstudio.com/gallery.html)
* [Steve's R Markdown Templates](https://github.com/svmiller/svm-r-markdown-templates)