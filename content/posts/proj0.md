---
title: "Project and Games"
date: 2020-03-23T08:12:13+08:00
draft: false
categories: [team, game]
tags: [project]
---

![](https://imgs.niusnews.com/upload/imgs/default/201910_Sharon/nine/nine11.jpg)

### 分組 (初稿)

1. 我們就爛:  吳晨瑄,  紀婷翊, <u>白敏蒨</u>, 吳姿儀
2. IU是天使: 沈怡姍, 蔡瑄, 曾柏恩, 彭長元, <u>陳慬瑜</u>, 林羿君, 高芷芃
3. 今日打老虎 明日打老母: <u>王品皓</u>,  <u>童政瑋</u>, 尹健璋,  黃紫瑄, 莊芸綺, 劉心雨
4. TBA: 陳心如,  <u>龍冠邑</u>, 黃奕淳
5. TBA: <u>吳振瑋</u>, 吳林晏, 黃宗元, 羅凱倫 
6. 神魔統軟:  <u>邵平遠</u>,  阮俊瑋, 謝旻倫, 謝昕龍, 王委凱
7. TBA: 康暐郡, <u>吳昱潔</u>, 高偉泰, 蕭冠鈞
8. TBA: <u>蘇羿豪</u>, 阮新宇, 李采晏, 林穎宏
9. TBA: <u>吳家萱</u>, 宋宸耘, 黃子娟, 郭郁琪. 主題：全臺灣(含離島)四大超商的分布 
10. 高粱王: 張軼堂, 劉群益, 林士玄, <u>林泓廷</u>
11. 鼠巴拉希: 陳貞佑, <u>葉庭</u>, 楊智凱, 林思佑, 廖政維, 施承翰
12. 賴宜婷: <u>賴宜婷</u>

Data Science Project Team

1. [Data science is a team sport.  Do you have the skills  to be a team player?](https://www.ibm.com/downloads/cas/NZRDABJV)
2. [How to Structure a Data Science Team: Key Models and Roles to Consider](https://www.altexsoft.com/blog/datascience/how-to-structure-data-science-team-key-models-and-roles/)