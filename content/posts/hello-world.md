---
title: "Week 1. Overview"
date: 2020-03-01T20:55:30+08:00
draft: false
tags: [why]
categories: [note]
---
![](https://akcp.kanfanba.com/wp-content/uploads/2020/01/%E8%99%9A%E6%A7%8B%E6%8E%A8%E7%90%86.jpg)


Welcome to R Games 2020!

### Why, What and How?

* Hype or not?: BD, AI,  Analytics, Infomatics/infometrics, DV and BI
* Data Science $\approx$ OCEMIC (acronym inspired by Dr. Lau's  [OSEMN](https://towardsdatascience.com/5-steps-of-a-data-science-project-lifecycle-26c50372b492)) 
* [CareerCast Best Jobs 2019](https://www.careercast.com/jobs-rated/2019-jobs-rated-report) 
* [Kaggle](https://www.kaggle.com/)

### R plus ...
 * R: [main](https://www.r-project.org/), [mirrors](https://cran.r-project.org/mirrors.html)
 * [Rstudio](https://rstudio.com/): [Download](https://rstudio.com/products/rstudio/download/#download)
 * Code editor and IDE: [notepad++](https://notepad-plus-plus.org/), [geany](https://www.geany.org/); [visual studio](https://code.visualstudio.com/), [atom](https://atom.io/) and [more](https://www.guru99.com/best-free-code-editors-windows-mac.html).
 
 
 
### Gaming
> 只有當人充分是人的時候，他才遊戲；只有當人遊戲的時候，他才完全是人。
> 
>  Man only plays when in the full meaning of the word he is a man, and he is only completely a man when he plays.
> 
 <cite>Johann Christoph Friedrich von Schiller.
  On the Aesthetic Education of Man (1794), Letter 15.</cite>