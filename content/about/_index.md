---
title: "About"
date: 2020-03-01T20:55:45+08:00
draft: false
tags: []
categories: [admin]
---
![雷姆](https://i.ytimg.com/vi/2ltofrl7IHo/maxresdefault.jpg)

### 初始設定

這是統計軟體與實務應用2020課程網頁。       Curator：Kno Tsao

資料的取得、面向與規模隨著網路發展超速成長。統計是資料科學中的一個重要主軸。本課程將透過引介統計軟體 R, Rstudio, 以及網站 R bloggers, Github, Kaggle 引發學生在資料科學分析與實作的第一步。教學計畫表 syllabus 部份的資料與內容可能將視課程進行狀況略做調整。

以上是官方的說法。真實的設定是  ——— **R games**. 

有興趣同學也可參考一下[2017版課網](https://ssar2017kno.wordpress.com/)。


### 課程資訊

* Curator: C. Andy Tsao  Office: SE A411.  Tel: 3520
* Lectures: Mon. **1000**-1200, Wed. **1400**-1500 @ AE B101
Office Hours:  Mon. 12:10-13:00, Thr. 15:10-16:00. @ SE A411 or by appointment. (Mentor Hour:  Tue/Thr 1200-1300. 也歡迎來，但若有導生來，則導生優先)
* TA Office Hours
  * Mon 1800~1900 陳學蒲 A414 email: 610811101@gms.ndhu.edu.tw (Course, R, Gaming (Video and Boardgame) Data)
  * Wed 1700~1900 何尚謙 A408 分機：3517 (Kaggle Data, Python)
  * Thu 1700~1900 劉雅涵 A408 分機：3517 (R, Text Mining, Flavour Net, Recipes)
* Prerequisites: Intro to Probability, Statistics (taken or taking)
